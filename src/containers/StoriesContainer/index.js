import React, { useEffect, useState } from "react";
import { getStories } from "../../apis/parsely";
import Header from "../../components/Header";
import Panel from "../../components/Panel";

const StoriesContainer = () => {
  const [stories, setStories] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    getStories()
      .then((data) => setStories(data))
      .catch((err) => setError(err));
  }, []);

  if (error) throw error;

  return (
    <div className="stories-container">
      <Header heading="Trending on Facebook" />
      <Panel stories={stories} />
    </div>
  );
};

export default StoriesContainer;
