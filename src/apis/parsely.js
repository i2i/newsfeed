import axios from "axios";
import {
  BASE_URL,
  APIKEY,
  APISECRET,
  PERIOD_START,
  SORT
} from "../constants/index";
import { getTopAuthors } from "../utils/helpers";

export const baseUrl = BASE_URL;
export const analyticsEndpoint = `${baseUrl}analytics/`;
export const getPosts = `${analyticsEndpoint}posts?`;

export const apiKey = APIKEY;
export const secret = APISECRET;
export const params = {
  period_start: PERIOD_START,
  sort: SORT
};

export const getStories = async () => {
  const stories = await axios
    .get(
      `${getPosts}apikey=${apiKey}&secret=${secret}&period_start=${params.period_start}&sort=${params.sort}`
    )
    .then(({ data }) => data.data && data.data)
    .catch(({ error }) => error);
  const topStories = stories ? getTopAuthors(stories) : null;
  return topStories;
};
