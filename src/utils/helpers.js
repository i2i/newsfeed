export function trimAuthorName(author) {
  const trimmedName = author.includes("|")
    ? author.slice(0, author.indexOf("|"))
    : author;
  return capitalizeAuthorName(trimmedName);
}

export function capitalizeAuthorName(author) {
  let nameArray = author.split(" ");
  let fixedName = [];
  nameArray.map((name) => {
    let capName = "";
    if (name.length > 1 && name !== "III") {
      capName = `${name[0].toUpperCase()}${name.substr(1).toLowerCase()}`;
    } else {
      capName = name;
    }
    fixedName.push(capName);
    return fixedName;
  });
  return fixedName.join(" ");
}

export const getTopAuthors = (stories) => {
  let storyArray = [];
  let authorsArray = [];
  for (let story of stories) {
    // We want five unique authors
    if (authorsArray.indexOf(trimAuthorName(story.author)) < 0) {
      authorsArray.push(trimAuthorName(story.author));
      storyArray.push({
        id: storyArray.length,
        title: story.title,
        author: trimAuthorName(story.author),
        fb_referrals: story.metrics.fb_referrals,
        views: story.metrics.views,
        image_url: story.thumb_url_medium,
        url: story.url
      });
    }
  }
  // The array is currently ordered by fb_referrals (the metric used to order the API results)
  // First we will take the first 5 with array.slice(), then we will sort those 5 by views
  return storyArray.slice(0, 5).sort((a, b) => b.views - a.views);
};
