import React from "react";
import classNames from "classnames";
import "bootstrap/dist/css/bootstrap.min.css";
import StoriesContainer from "./containers/StoriesContainer";

export const App = () => {
  return (
    <main className="container-fluid">
      <section
        className={classNames("row", "col-sm-12", "col-md-9", "col-xl-7")}
      >
        <StoriesContainer />
      </section>
    </main>
  );
};
