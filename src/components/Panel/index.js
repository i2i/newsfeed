import React from "react";
import classNames from "classnames";
import Story from "../../components/Story";
import "./panel.css";

const Panel = ({ stories }) => {
  return (
    <div className={classNames("card-panel", "panel")}>
      <div className="panel-body">
        <div className="card">
          <ul className={classNames("list-unstyled", "list-trending")}>
            {stories ? (
              stories.map((story) => (
                <Story
                  title={story.title}
                  author={story.author}
                  image_url={story.image_url}
                  url={story.url}
                  id={story.id}
                  key={story.id}
                />
              ))
            ) : (
              <p>There was a problem retrieving stories.</p>
            )}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Panel;
