import React from "react";
import "./card.css";

export const CardLabel = ({ author }) => (
  <div className="col-md-12">
    <h3 className="card-label-author">{author}</h3>
  </div>
);
