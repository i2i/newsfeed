import React from "react";
import classNames from "classnames";
import "./card.css";

export const CardImage = ({ alt, src }) => {
  return (
    <div className="col-md-12">
      <figure
        className={classNames(
          "photo",
          "layout-horizontal",
          "letterbox-style-blur"
        )}
      >
        <img alt={alt} src={src} />
      </figure>
    </div>
  );
};
