import React from "react";
import "./card.css";

export const CardHeadline = ({ href, title }) => {
  return (
    <div className="col-md-12">
      <h4 className="card-headline">
        <a href={href}>{title}</a>
      </h4>
    </div>
  );
};
