import React from "react";
import classNames from "classnames";
import "./header.css";

const Header = ({ heading }) => {
  return (
    <div className={classNames("block-title", "col-md-12")}>
      <div className="block-title-inner">
        <h3>{heading}</h3>
      </div>
    </div>
  );
};

export default Header;
