import React from "react";
import classNames from "classnames";
import { CardImage, CardLabel, CardHeadline } from "../Card";
import "./story.css";

const Story = ({ title, author, image_url, url, id }) => {
  return (
    <li key={id}>
      <div className="row col-md-12 story-row">
        <div className={classNames("col-lg-2", "col-md-3", "col-sm-12")}>
          <CardImage alt={title} src={image_url} />
        </div>
        <div
          className={classNames("row", "col-lg-10", "col-md-9", "col-sm-12")}
        >
          <CardLabel author={author} />
          <CardHeadline href={url} title={title} />
        </div>
      </div>
    </li>
  );
};

export default Story;
