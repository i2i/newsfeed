# Trending Authors Module

## Implementation Notes

This module uses data from the Analytics endpoint on Parse.ly API to build a list of 5 authors with the highest number of Facebook referrals over the last 24 hours. For each author, we display their article's thumbnail and headline (linked to the article). In the event that one author has multiple articles in the current list of top 5 Facebook referrals, we will only get the article that has the highest number of referrals; otherwise, the list could end up with duplicates of a single author. Once the list is built, it is sorted by the total number of page views (descending) on each article.

The time period for results and the sort criteria for the api call can be changed by editing their corresponding values in `./constants/index.js`.

### Technical Discovery

I took the following steps prior to beginning development work:

1. Tested API calls to Parse.ly using Postman
1. Used results of Postman tests to determine shape of response object
1. Chose React.js for building the front end components
1. API call to use axios for Promise-based GET requests
1. Data to be injected into module using React's useEffect hook
1. Front end styling to match current styling used on nola.com

### Development

1. Spun up development environment using Node.js
1. Created new React.js app using Create React App
1. Installed package axios from npmjs
1. Installed bootstrap and react-bootstrap for styling
1. Created script for handling API calls and utility file for applying business logic rules to results
1. Created components for displaying results in list format

### Running the app (dev mode)

1. Clone the repo and run `yarn install` in the project directory
1. Open ./src/constants/index.js and add values for the api key and secret
1. To run the app in development mode, run `yarn start` in the project directory
1. The app can be viewed in a browser at [http://localhost:3000](http://localhost:3000)
1. Webpack will reload the page if any edits are made
1. If you get the message "There was a problem retrieving stories", confirm that you entered the api key and secret as string values for the constants `APIKEY` and `APISECRET`

### Building for production

1. Running `yarn build` builds the app for production to the `build` folder
1. The app is ready to be deployed
1. See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information

### Wrap-up Notes

1. In a real-world app with more time, I would probably handle the API call and business logic in a middleware or backend layer. Putting it in the React app works, but it steps outside of the MVC model, where the React app is purely the View component and is kept "dumb".
1. The helper file (/src/utils/helpers.js) has methods to prettify the author's name (in the API results, it is in the format "JOHN SMITH | STAFF WRITER"):
   - `trimAuthorName()` removes the pipe "|" and title; this is not something I would usually do on an actual project unless it was unavoidable; it is too reliant on a given shape of string.
   - `capitalizeAuthorName()` returns the name as John Smith; you will notice a rather odd line that searches for the substring "III" in a name... this is a very sloppy and unadvisable method, but I used it because one of the authors whose name was in my results was FAIMON A. ROBERTS III, which was coming out as "Faimon A. Roberts Iii". I didn't have time to fully implement a true name capitalization function, so I just took care of it using a Whack-a-Mole approach. Again, not something I would implement in an actual project.
1. The classnames (and their associated style rules) are pretty messy and do not follow any best practices or patterns. I left styling until last and was trying to mimic the look and feel of content on nola.com. The first thing I would probably do if this were an actual project is yank out the DOM structure and styles and rebuild it to properly integrate into the site.
1. Result sets from Parse.ly's Analytics API do not include an ID on each article. I added a crude method of adding a unique ID to each article; this is used to provide values for the `key` attribute on iterated `Story` components and `<li>` items.
1. The list of authors/stories that are displayed may not match the results in the "MOST POPULAR" section of nola.com; they also may not match a list of Facebook Referrals in the Parse.ly control panel. This is because he results in this app are culled from the number of fb_referrals (first), then sorted according to their total pageviews (second). Additionally, the removal of duplicate articles by the same author could cause this list to be different from those on the site or in Parse.ly.
1. I ran out of time to include unit testing, which would need to be included in a real project. I would add unit testing with Jest and React Testing Library.
